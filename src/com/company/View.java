package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class View {

    private Presenter presenter;
    private Model model;

    private String info = "Нажмите 1 , если вам нужен калькулятор"+ "\n" +
            "Нажмите 2 , если вам нужен конвертер" +"\n"+
            "Нажмите help, если вам нужна помощь" + "\n"+
            "Нажмите exit, чтобы выйти ";

    private BufferedReader reader;
    private boolean flow = true;

    public View() {
        System.out.println(info);
        presenter = new Presenter(this);
    }

    public View(Model model) {
        this.model = model;
    }

    public void run() throws IOException {
        String str = "";
       while (flow){
           reader = new BufferedReader(new InputStreamReader(System.in));
           str = reader.readLine();
           presenter.choose(str);
           run();
       }
    }


    public void setFlow(boolean flow) {
        this.flow = flow;
    }

    public String getInfo() {
        return info;
    }
    public void close(){
        setFlow(false);
    }
}
