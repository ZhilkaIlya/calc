package com.company;

import java.io.IOException;

public class Presenter {

    private View view;
    private Model model;


    public Presenter(View view) {
        this.view = view;
        model = new Model(this);
    }

    public void choose(String string) throws IOException {
        switch (string){
            case "1":
                System.out.println("Вы выбрали калькулятор");
                model.calcResult();
                break;
            case "2":
                System.out.println("Вы выбрали конвертер");
                break;
            case "help":
                System.out.println(view.getInfo());
                break;
            case "exit":
                view.close();
                break;
        }
    }
}
