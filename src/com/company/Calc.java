package com.company;

public class Calc {

    private Model model;

    public Calc(Model model) {
        this.model = model;
    }

    public <T, E> void chooseType(T firstValue, E secondValue, String str) {
        String result = "";
        if (Double.class.isInstance(firstValue) || Double.class.isInstance(secondValue)){
            double firstNum = 0.0;
            double secondNum = 0.0;
            Number first = (Number) firstValue;
            Number second = (Number) secondValue;
            firstNum = first.doubleValue();
            secondNum = second.doubleValue();
           result = switchDoubleType(firstNum,secondNum,str);
            System.out.println(result);

        }else if (Integer.class.isInstance(firstValue) && Integer.class.isInstance(secondValue)){
            result = switchIntType(firstValue,secondValue,str);
        }
        System.out.println(result);
    }


    private  <T, E> String switchDoubleType(T firstValue, E secondValue, String string) {
        String str = "";
        double doubleResult = 0.0;
        if (Double.class.isInstance(firstValue) || Double.class.isInstance(secondValue)) {
            switch (string) {
                case "+":
                    doubleResult = (double) firstValue + (double) secondValue;
                    break;
                case "-":
                    doubleResult = (double) firstValue - (double) secondValue;
                    break;
                case "*":
                    doubleResult = (double) firstValue * (double) secondValue;
                    break;
                default:
                    double secondNum = 0.0;
                    Number second = (Number) secondValue;
                    secondNum = second.doubleValue();
                    if (secondNum != 0) {
                        doubleResult = (double) firstValue / (double) secondValue;
                        str = "" + doubleResult;
                    } else {
                        System.out.println("На ноль делить нельзя");
                    }
            }
            str = "" + doubleResult;
        }
        return str;
    }

    private  <T, E> String switchIntType(T firstValue, E secondValue, String string) {
        String str = "";
        int intResult = 0;
            switch (string) {
                case "+":
                    intResult = (int) firstValue + (int) secondValue;
                    break;
                case "-":
                    intResult = (int) firstValue - (int) secondValue;
                    break;
                case "*":
                    intResult = (int) firstValue * (int) secondValue;
                    break;
                default:
                    double secondNum = 0.0;
                    Number second = (Number) secondValue;
                    secondNum = second.intValue();
                    if (secondNum != 0) {
                        intResult = (int) firstValue / (int) secondValue;
                    }else {
                        System.out.println("На ноль делить нельзя");
                    }
            }

            str = "" + intResult;
        return str;
    }
}
