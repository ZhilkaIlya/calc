package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Model {

    private Presenter presenter;
    private View view;
    private Calc calc;

    public Model(Presenter presenter) {
        this.presenter = presenter;
        view = new View(this);
        calc = new Calc(this);
    }

    public void calcResult() throws IOException {
        String operation = operation();
        if (operation.equals("help")) {
            presenter.choose(operation);
        } else {
            Number firstNumber = firstNumber();
            Number secondNumber = secondNumber();
            calc.chooseType(firstNumber, secondNumber, operation);
           calcResult();
        }
    }


        private String operation() throws IOException {
            System.out.println("You can choose only + - * / or help");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String str = null;
            try {
                str = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (str.equals("/") || str.equals("*") || str.equals("+") || str.equals("-") || str.equals("help")) {
                return str;
            } else {
                return operation();
            }
        }
    private Number firstNumber () throws IOException {
        Number number;
        System.out.println("Input your first number");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String str = reader.readLine();
        if (isInteger(str)) {
            number = Integer.parseInt(str);
            return number;
        }else if (isDouble(str)){
            number = Double.parseDouble(str);
            return number;
        }else {
            System.out.println("You can input only number");
            return firstNumber();

        }

    }

    private Number secondNumber () throws IOException {
        Number number;
        System.out.println("Input your second number");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String str = reader.readLine();
        if (isInteger(str)) {
            number = Integer.parseInt(str);
            return number;
        }else if (isDouble(str)){
            number = Double.parseDouble(str);
            return number;
        }else {
            System.out.println("You can input only number");
            return firstNumber();

        }
    }

    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }
    private static boolean isDouble(String s) {
        try {
            Double.parseDouble(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }



}
